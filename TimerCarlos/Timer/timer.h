#ifndef TIMER_H
#define TIMER_H

#include <stdlib.h>
#include <stdbool.h>

#define RELAYACTIVADO 1
#define RELAYDESACTIVADO 0

unsigned long comenzarMedicion(int);
bool detectorFlanco(int, int);

int *medicionValida;

#endif
