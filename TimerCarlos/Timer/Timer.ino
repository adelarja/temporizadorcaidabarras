#define PINENTRADA 1

#include "timer.h"

extern int *medicionValida;
unsigned long tiempo = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(PINENTRADA, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  tiempo = comenzarMedicion(PINENTRADA); 

  if((*medicionValida) == 1){
    Serial.print(tiempo);
    Serial.print(" mS");
    Serial.print("\n");
    *medicionValida = 0;
  }

}
