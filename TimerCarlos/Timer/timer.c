#include "timer.h"

extern int *medicionValida;

bool detectorFlanco(int pin, int estado){

  bool retorno = false;
  
  if (digitalRead(pin) == estado){

    delay(30);

    if (digitalRead(pin) == estado)
      retorno = true;
    
  }

  return retorno;
  
}

unsigned long comenzarMedicion(int pin){

  unsigned long tiempoInicio = 0;
  unsigned long tiempoFinalizacion = 0;
  
  if (detectorFlanco(pin, RELAYACTIVADO)){

    tiempoInicio = millis();

    while(!(detectorFlanco(pin, RELAYDESACTIVADO)))
      ;

    tiempoFinalizacion = millis();

    *medicionValida = 1;
    
  }

  return (tiempoFinalizacion - tiempoInicio);
    
}
