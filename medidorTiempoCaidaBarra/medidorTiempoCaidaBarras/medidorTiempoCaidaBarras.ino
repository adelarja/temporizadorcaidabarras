int pinSPB = 7;
int pinL1B = 8;
unsigned long tiempo;

bool tomarEstadoSenales(int, int);
unsigned long medirTiempoCaidaBarras(void);

void setup() {
  // put your setup code here, to run once:
  pinMode(pinSPB, INPUT);
  pinMode(pinL1B, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  tiempo = medirTiempoCaidaBarras(); 

  if(tiempo > 50){
    Serial.print(tiempo);
    Serial.print(" mS");
    Serial.print("\n");
    tiempo = 0;
  }

}

bool tomarEstadoSenales(int pin, int nivel){

  bool state = false;

  if(digitalRead(pin) == nivel){

    delay(30);
        
    if(digitalRead(pin) == nivel){
      state = true;
    }
    
  }

  return state;
  
}

unsigned long medirTiempoCaidaBarras(){

  unsigned long tiempoInicio=0;
  unsigned long tiempoFin=0;
  
  bool SPB = false;
  bool L1B = false;

  SPB = tomarEstadoSenales(pinSPB, HIGH);
  
  if(SPB){

    tiempoInicio = millis();

    while(SPB){ 
      
      L1B = tomarEstadoSenales(pinL1B, HIGH);
          
      if(L1B){
        tiempoFin = millis();
        L1B = false;
        SPB = false;
      }
      
    }
  }

  return (tiempoFin - tiempoInicio + 60);
  
}
